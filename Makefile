CC    = gcc
PROG  = prog
OPCJE = -W -Wall -O0 -std=gnu99 -pedantic -pedantic-errors



$(PROG): $(PROG).c Makefile
	$(CC) $(OPCJE) $< -o $@




.PHONY: clean mem sz

sz:
	git add -A
	git commit -m "... awaryjne publikowanie ..."
	git push --force --all ; sync
	sync

mem: ./$(PROG)
	valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes --tool=memcheck ./$(PROG)

clean:
	@rm -f ./'$(PROG)' ./*.o
